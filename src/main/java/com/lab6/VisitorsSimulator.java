package com.lab6;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class VisitorsSimulator {
    private List<String> mockCustomerNames = new ArrayList<>();
    private List<String> mockStreets = new ArrayList<>();

    private List<Customer> customers = new ArrayList<>();

    public VisitorsSimulator() {
        mockCustomerNames.add("Vlad");
        mockCustomerNames.add("Ivan");
        mockCustomerNames.add("Oleh");
        mockCustomerNames.add("Nazar");
        mockCustomerNames.add("Serhii");
        mockCustomerNames.add("Olexandra");
        mockCustomerNames.add("Petro");
        mockCustomerNames.add("Mariana");
        mockCustomerNames.add("Kira");
        mockCustomerNames.add("Olena");

        mockStreets.add("Stepana Bandery st.");
        mockStreets.add("Lukasha st.");
        mockStreets.add("Horodotska st.");
        mockStreets.add("Ivana Franka st.");
        mockStreets.add("Lvivska st.");
        mockStreets.add("Sryiska st.");
        mockStreets.add("Taranovskogo st.");
        mockStreets.add("Zelena st.");
        mockStreets.add("Baturyna st.");
        mockStreets.add("Chornovola st.");
        mockStreets.add("Saharova st.");
    }

    public void simulateCustomersFlow(Pizzeria pizzeria, int customersCount) {
        for (int i = 0; i < customersCount; ++i) {
            Random random = new Random();
            Customer customer = new Customer(mockCustomerNames.get(random.nextInt(mockCustomerNames.size())));
            customer.visit(pizzeria);

            var customerPizzas = new HashMap<Pizza, Integer>();
            int pizzasCount = random.nextInt(1, 5);
            for (int j = 0; j < pizzasCount; ++j) {
                customerPizzas.put(pizzeria.getRandomPizza(), random.nextInt(1, 5));
            }

            // generate address
            String generatedAddress = "Lviv, "
                    + mockStreets.get(random.nextInt(mockStreets.size()))
                    + ", " + random.nextInt(1, 101);

            // generate order with random time
            Order customerOrder = new Order();
            customerOrder.setAddress(generatedAddress);
            customerOrder.setPizzas(customerPizzas);
            customerOrder.setExpectedDeliveryTime(LocalDateTime.now().plusMinutes(random.nextInt(-20, 20)));

            customer.makeOrder(customerOrder);

            customers.add(customer);

            try {
                Thread.sleep(random.nextInt(100, 1000));
            } catch (InterruptedException e) {
                System.out.println("Thread sleep exception");
            }
        }
    }

    public Customer getCustomerById(int id) {
        return customers.get(id);
    }
}
