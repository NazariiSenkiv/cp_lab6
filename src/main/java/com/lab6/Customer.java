package com.lab6;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Customer {
    private String name;
    private List<Integer> orderIds = new ArrayList<>();

    private Pizzeria pizzeria;

    public Customer(String name) {
        this.name = name;
    }
    public void setPizzeria(Pizzeria pizzeria) {
        this.pizzeria = pizzeria;
    }
    public void addOrder(int orderId) {
        orderIds.add(orderId);
    }

    public List<Integer> getOrderIds() {
        return orderIds;
    }
    public String getName() {
        return name;
    }
    public void makeOrder(String address, Map<Pizza, Integer> pizzas) {
        orderIds.add(pizzeria.addOrder(address, pizzas));
    }
    public void makeOrder(Order order) {
        orderIds.add(pizzeria.addOrder(order));
    }

    public void visit(Pizzeria pizzeria) {
        pizzeria.addCustomer(this);
    }

    @Override
    public String toString() {
        return "(Customer| "+getName()+", orderIds: "+getOrderIds()+")";
    }
}
