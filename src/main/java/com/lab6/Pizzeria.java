package com.lab6;

import com.lab6.json.JsonPizzaFileParser;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public class Pizzeria {
    private String name;

    private List<Customer> customers = new ArrayList<>();
    private List<Order> orders = new ArrayList<>();
    private List<Pizza> pizzas;

    private long expectedDeliveryTimeMinutes = 15;

    public Pizzeria(String name, String pizzasFilePath) {
        this.name = name;

        pizzas = JsonPizzaFileParser.parseFile(pizzasFilePath);
    }

    public void addCustomer(Customer customer) {
        customer.setPizzeria(this);
        this.customers.add(customer);
    }

    //returns order id
    public int addOrder(String address, Map<Pizza, Integer> pizzas) {
        var order = new Order();
        int id = orders.size();

        order.setId(id);
        order.setAddress(address);
        order.setPizzas(new HashMap<>(pizzas));
        order.setExpectedDeliveryTime(LocalDateTime.now().plusMinutes(expectedDeliveryTimeMinutes));

        this.orders.add(order);

        return id;
    }

    public int addOrder(Order order) {
        var newOrder = new Order();
        int id = orders.size();

        newOrder.setId(id);
        newOrder.setAddress(order.getAddress());
        newOrder.setPizzas(new HashMap<>(order.getPizzas()));
        newOrder.setExpectedDeliveryTime(order.getExpectedDeliveryTime());

        this.orders.add(newOrder);

        return id;
    }
    public void addPizza(Pizza pizza) {
        this.pizzas.add(pizza);
    }
    public long getExpectedDeliveryTimeMinutes() {
        return expectedDeliveryTimeMinutes;
    }
    public void setExpectedDeliveryTimeMinutes(long expectedDeliveryTimeMinutes) {
        this.expectedDeliveryTimeMinutes = expectedDeliveryTimeMinutes;
    }

    // returns pizza or null
    public Pizza getPizzaByName(String pizzaName) {
        Pizza foundPizza = null;
        try {
            foundPizza = pizzas.stream().filter(p -> p.getName().equals(pizzaName)).findFirst().get();
        } catch (Exception exception) {
            return null;
        }
        return foundPizza;
    }

    public Customer getCustomerByName(String customerName) {
        Customer foundCustomer = null;
        try {
            foundCustomer = customers.stream().filter(p -> p.getName().equals(customerName)).findFirst().get();
        } catch (Exception exception) {
            return null;
        }
        return foundCustomer;
    }

    public Pizza getRandomPizza() {
        Random random = new Random();
        return pizzas.get(random.nextInt(pizzas.size()));
    }

    public List<Order> getOrdersSortedByDeliveryTime() {
        return orders.stream()
                .sorted((el1, el2) -> el1.getExpectedDeliveryTime().compareTo(el2.getExpectedDeliveryTime()))
                .collect(Collectors.toList());
    }

    public List<Customer> getCustomersOrderedMoreTwoPizzas() {
        return customers.stream()
                        .filter(c -> orders.stream()
                                           .filter(o -> c.getOrderIds().contains(o.getId()))
                                           .mapToInt(Order::getTotalPizzasCount).sum()
                                > 2)
                        .collect(Collectors.toList());
    }

    public int getCustomersCountOrderedPizza(String pizzaName) {
        if (!pizzas.stream().map(Pizza::getName).toList().contains(pizzaName)) {
            return 0;
        }

        return (int)customers.stream()
                             .filter(c -> orders.stream()
                                                .filter(o -> c.getOrderIds().contains(o.getId()))
                                                .anyMatch(o -> o.getPizzas().containsKey(getPizzaByName(pizzaName))))
                             .count();
    }

    public int getCustomerMaxBoughtPizzas(Customer customer) {
        if (customer == null) return 0;
        var unitedCustomerOrder = orders.stream().filter(o -> customer.getOrderIds().contains(o.getId()))
                                                 .reduce(Order::add).get();
        return unitedCustomerOrder.getPizzas().values().stream()
                                                       .mapToInt(integer -> integer)
                                                       .max().getAsInt();
    }

    public Map<Pizza, List<Customer>> getPizzasCustomers() {
        Map<Pizza, List<Customer>> pizzasCustomers = new HashMap<>();

        for (var pizza: pizzas) {
            List<Customer> pizzaCustomers =
                    customers.stream()
                             .filter(c -> orders.stream()
                                                .filter(o -> c.getOrderIds().contains(o.getId()))
                                                .anyMatch(o -> o.getPizzas().containsKey(pizza)))
                             .toList();

            pizzasCustomers.put(pizza, pizzaCustomers);
        }

        return pizzasCustomers;
    }

    public List<Order> getOverdueOrders() {
        return orders.stream()
                     .filter(o -> LocalDateTime.now().compareTo(o.getExpectedDeliveryTime()) > 0)
                     .collect(Collectors.toList());
    }
}
