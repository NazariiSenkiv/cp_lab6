package com.lab6;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Pizzeria mamamiaPizza = new Pizzeria("MaMaMia Pizza", "pizzas.json");


        // Test customer
        Customer mario = new Customer("Mario");
        var marioPizzas = new HashMap<Pizza, Integer>();
        marioPizzas.put(mamamiaPizza.getPizzaByName("Margherita"), 3);
        marioPizzas.put(mamamiaPizza.getPizzaByName("Neapolitan Pizza"), 5);
        marioPizzas.put(mamamiaPizza.getPizzaByName("Greek Pizza"), 2);
        marioPizzas.put(mamamiaPizza.getPizzaByName("Tomatoes"), 2);

        mario.visit(mamamiaPizza);
        mario.makeOrder("Rome", marioPizzas);
        // ~Test customer

        var simulator = new VisitorsSimulator();
        simulator.simulateCustomersFlow(mamamiaPizza, 5);

        // Test customer second order
        var marioPizzas2 = new HashMap<Pizza, Integer>();
        marioPizzas2.put(mamamiaPizza.getPizzaByName("Margherita"), 2);
        marioPizzas2.put(mamamiaPizza.getPizzaByName("Neapolitan Pizza"), 3);
        marioPizzas2.put(mamamiaPizza.getPizzaByName("Tomatoes"), 1);

        mario.makeOrder("Rome", marioPizzas2);
        // ~Test customer second order

        Scanner scanner = new Scanner(System.in);

        ConsoleTerminal terminal = new ConsoleTerminal(mamamiaPizza, scanner);
        terminal.start();

        serializeOverdueOrders(mamamiaPizza);

        var overdue = loadSerializedOverdueOrders();

        scanner.close();
    }

    public static void serializeOverdueOrders(Pizzeria pizzeria) {
        try
        {
            FileOutputStream fos = new FileOutputStream("overdueOrders.txt");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(pizzeria.getOverdueOrders());
            oos.close();
            fos.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

    public static List<Order> loadSerializedOverdueOrders() {
        var overdueOrders = new ArrayList<Order>();
        try
        {
            FileInputStream fis = new FileInputStream("overdueOrders.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);

            overdueOrders = (ArrayList<Order>) ois.readObject();

            ois.close();
            fis.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
            return null;
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
            return null;
        }

        return overdueOrders;
    }
}
