package com.lab6;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class Order implements Serializable {
    private int id;
    private String address;
    private Map<Pizza, Integer> pizzas = new HashMap<>();
    private LocalDateTime expectedDeliveryTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Map<Pizza, Integer> getPizzas() {
        return pizzas;
    }

    public void setPizzas(Map<Pizza, Integer> pizzas) {
        this.pizzas = pizzas;
    }
    public void addPizzas(Pizza pizza, int count) {
        this.pizzas.put(pizza, count);
    }

    public LocalDateTime getExpectedDeliveryTime() {
        return expectedDeliveryTime;
    }

    public void setExpectedDeliveryTime(LocalDateTime expectedDeliveryTime) {
        this.expectedDeliveryTime = expectedDeliveryTime;
    }

    public int getTotalPizzasCount() {
        return pizzas.values().stream().mapToInt(integer -> integer).sum();
    }

    public Order add(Order other) {
        var newOrder = new Order();

        newOrder.setPizzas(new HashMap<>(pizzas));

        for (var entry: other.getPizzas().entrySet()) {
            Pizza pizza = entry.getKey();

            if (newOrder.pizzas.containsKey(pizza)) {
                int oldCount = newOrder.pizzas.get(pizza);
                newOrder.pizzas.put(pizza, oldCount + entry.getValue());
            }
            else {
                newOrder.pizzas.put(pizza, entry.getValue());
            }
        }

        return newOrder;
    }

    @Override
    public String toString() {
        return "(Order| id:"+getId()+", addr:"+getAddress()+", expTime:"
                +getExpectedDeliveryTime()+", pizzas:"+getPizzas()+")";
    }
}
