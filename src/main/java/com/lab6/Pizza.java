package com.lab6;

import com.lab6.json.JsonPizzaFileParser;

import java.util.ArrayList;
import java.util.List;

public abstract class Pizza {
    protected String name;
    protected float weight;
    protected float price;
    protected List<String> components = new ArrayList<>();

    public String getName() {
        return name;
    }
    public float getWeight() {
        return weight;
    }
    public float getPrice() {
        return price;
    }
    public List<String> getComponents() {
        return new ArrayList<>(components);
    }

    @Override
    public String toString() {
        return getName();
    }
}
