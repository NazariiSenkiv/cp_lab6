package com.lab6.json;

import com.lab6.Pizza;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class JsonPizzaFileParser {
    public static List<Pizza> parseFile(String filePath) {
        List<Pizza> pizzas = new ArrayList<>();

        var parser = new JSONParser();

        try (FileReader reader = new FileReader(filePath)) {
            JSONArray pizzasJsonArray = (JSONArray) parser.parse(reader);

            for(Object pizza : pizzasJsonArray) {
                JSONObject pizzaJsonObject = (JSONObject) pizza;

                pizzas.add(ParsedPizza.parse(pizzaJsonObject));
            }
        } catch (Exception e) {
            System.out.println("File parse error in class HouseholdApplianceJsonParser, " + e);
        }

        return pizzas;
    }
    public static class ParsedPizza extends Pizza implements Serializable {

        public static Pizza parse(JSONObject pizzaJson) {
            var parsedPizza = new ParsedPizza();

            parsedPizza.setName(pizzaJson.get("name").toString())
                    .setWeight((float) (double)pizzaJson.get("weight"))
                    .setPrice((float) (double)pizzaJson.get("price"))
                    .setComponents((List<String>) pizzaJson.get("components"));

            return parsedPizza;
        }

        private ParsedPizza setName(String name) {
            this.name = name;
            return this;
        }
        private ParsedPizza setWeight(float weight) {
            this.weight = weight;
            return this;
        }
        private ParsedPizza setPrice(float price) {
            this.price = price;
            return this;
        }
        private ParsedPizza setComponents(List<String> components) {
            this.components = components;
            return this;
        }
        private ParsedPizza addComponent(String component) {
            this.components.add(component);
            return this;
        }
    }
}
