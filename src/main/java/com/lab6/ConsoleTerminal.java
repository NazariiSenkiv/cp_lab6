package com.lab6;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class ConsoleTerminal {
    private Pizzeria pizzeria;
    private Scanner scanner;
    int taskCount = 6;

    public ConsoleTerminal(Pizzeria pizzeria, Scanner scanner) {
        this.pizzeria = pizzeria;

        this.scanner = scanner;
    }

    private void showTaskResult(int num) {
        switch (num) {
            case 1:
                printSortedByDelivery();
                break;
            case 2:
                printCustomersWithMoreThanTwoPizzas();
                break;
            case 3:
                System.out.println("Please, enter pizza name:");
                String pizzaName;

                pizzaName = scanner.next();

                printPizzaOrdersCount(pizzaName);
                break;
            case 4:
                System.out.println("Please, enter customer name:");
                String customerName;

                customerName = scanner.next();

                printCustomerMaxBoughtPizza(pizzeria.getCustomerByName(customerName));
                break;
            case 5:
                printAllPizzaCustomers();
                break;
            case 6:
                printOverdueOrders();
                break;
        }
    }
    public void start() {
        while (true) {
            while (true) {
                System.out.println("Please, choose task num(from 1 to " + taskCount + "): ");

                var num = scanner.nextInt();
                if (num > 0 && num <= taskCount) {
                    showTaskResult(num);
                    break;
                }
                System.out.println("Sorry, num is in incorrect range");
            }
            System.out.println("Continue?(yes/no)");

            String answer = scanner.next();
            if (answer.toLowerCase().equals("no")) {
                break;
            }
        }
    }

    void printSortedByDelivery() {
        var sortedByDelivery = pizzeria.getOrdersSortedByDeliveryTime();

        System.out.println("\n\nOrders sorted by delivery time:");
        System.out.println(sortedByDelivery);
    }

    void printCustomersWithMoreThanTwoPizzas() {
        var withMoreThanTwoPizzas = pizzeria.getCustomersOrderedMoreTwoPizzas();

        System.out.println("\n\nCustomers with more than two pizzas:");
        System.out.println(withMoreThanTwoPizzas);
    }

    void printPizzaOrdersCount(String pizzaName) {
        int pizzaOrdersCount = pizzeria.getCustomersCountOrderedPizza(pizzaName);

        System.out.println("\n\nPizza \""+pizzaName+"\" was ordered in " + pizzaOrdersCount+ " orders");
    }

    void printCustomerMaxBoughtPizza(Customer customer) {
        int customerMaxBoughtPizza = pizzeria.getCustomerMaxBoughtPizzas(customer);

        if (customer != null) {
            System.out.println("\n\n"+customer.getName()+"'s max bought pizza count = " + customerMaxBoughtPizza);
        }
        else {
            System.out.println("\n\nNo such customer");
        }
    }

    void printAllPizzaCustomers() {
        var pizzasCustomers = pizzeria.getPizzasCustomers();

        System.out.println("\n\nAll pizza customers:");
        System.out.println(pizzasCustomers);
    }

    void printOverdueOrders() {
        var overdueOrders = pizzeria.getOverdueOrders();

        System.out.println("\n\nOverdue orders:");
        overdueOrders.forEach(o ->
                System.out.println(o + " overdue = "
                        + ChronoUnit.MINUTES.between(o.getExpectedDeliveryTime(), LocalDateTime.now()) + " min "
                        + ChronoUnit.SECONDS.between(o.getExpectedDeliveryTime(), LocalDateTime.now()) % 60 + " sec")
        );
    }
}
